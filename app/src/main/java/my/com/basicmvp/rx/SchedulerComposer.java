package my.com.basicmvp.rx;

import my.com.basicmvp.domain.executor.PostExecutionThread;
import my.com.basicmvp.domain.executor.ThreadExecutor;
import rx.Observable;
import rx.Single;
import rx.schedulers.Schedulers;

/**
 * Created by Andreas on 6/3/2017.
 */

public class SchedulerComposer {

    private final ThreadExecutor mThreadExecutor;
    private final PostExecutionThread mPostExecutionThread;
    private final Observable.Transformer mSchedulerComposer;

    public SchedulerComposer(ThreadExecutor mThreadExecutor, PostExecutionThread mPostExecutionThread) {
        this.mThreadExecutor = mThreadExecutor;
        this.mPostExecutionThread = mPostExecutionThread;
        this.mSchedulerComposer = create();
    }

    @SuppressWarnings("unchecked")
    public <T> Observable.Transformer<T, T> get() {
        return mSchedulerComposer;
    }

    @SuppressWarnings("unchecked")
    private <T> Observable.Transformer<T, T> create() {
        return new Observable.Transformer<T, T>() {
            @Override
            public Observable<T> call(Observable<T> tObservable) {
                return tObservable.subscribeOn(Schedulers.from(mThreadExecutor))
                        .observeOn(mPostExecutionThread.getScheduler(), true);
            }
        };
    }
}
