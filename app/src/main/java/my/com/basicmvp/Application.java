package my.com.basicmvp;

import my.com.basicmvp.di.component.CommonComponent;
import my.com.basicmvp.di.component.DaggerCommonComponent;
import my.com.basicmvp.di.module.CommonModule;
import timber.log.Timber;

/**
 * Created by Andreas on 6/3/2017.
 */

public class Application extends android.app.Application {

    private CommonComponent mCommonComponent;

    public Application() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());
        initCommonComponent();
    }

    public CommonComponent getmCommonComponent() {
        return mCommonComponent;
    }

    private void initCommonComponent() {
        mCommonComponent = DaggerCommonComponent.builder()
                .commonModule(new CommonModule(this))
                .build();
    }
}
