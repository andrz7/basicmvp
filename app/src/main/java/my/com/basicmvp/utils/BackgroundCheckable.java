package my.com.basicmvp.utils;

import android.support.v4.app.FragmentActivity;

/**
 * Created by kk-mb-007 on 6/14/17.
 */

public interface BackgroundCheckable {

    FragmentActivity getFragmentActivity();

    void clearSessionData();

    boolean isBackground();

}
