package my.com.basicmvp.utils;

import rx.Subscription;

/**
 * Created by Andreas on 6/4/2017.
 */

public class RxUtils {

    public static void unsubscribeAll(Subscription... subscriptions) {
        for (Subscription subscription : subscriptions) {
            if (subscription != null) subscription.unsubscribe();
        }
    }
}
