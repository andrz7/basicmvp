package my.com.basicmvp.domain.executor;

import java.util.concurrent.Executor;

import rx.Scheduler;

/**
 * Created by Andreas on 6/4/2017.
 */

public interface PostExecutionThread {
    Scheduler getScheduler();
}
