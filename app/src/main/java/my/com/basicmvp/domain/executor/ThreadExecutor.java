package my.com.basicmvp.domain.executor;

import java.util.concurrent.Executor;

/**
 * Created by Andreas on 6/4/2017.
 */

public interface ThreadExecutor extends Executor {
}
