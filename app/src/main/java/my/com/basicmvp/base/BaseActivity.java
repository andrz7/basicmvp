package my.com.basicmvp.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.inject.Inject;

import my.com.basicmvp.di.component.ActivityComponent;
import my.com.basicmvp.rx.SchedulerComposer;
import my.com.basicmvp.utils.BackgroundCheckable;

/**
 * Created by kk-mb-007 on 6/14/17.
 */

public class BaseActivity extends AppCompatActivity implements BackgroundCheckable, ErrorHandler{

    @Inject
    protected SchedulerComposer schedulerComposer;

    private ActivityComponent mActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public FragmentActivity getFragmentActivity() {
        return null;
    }

    @Override
    public void clearSessionData() {

    }

    @Override
    public boolean isBackground() {
        return false;
    }

    @Override
    public void warning(SAXParseException exception) throws SAXException {

    }

    @Override
    public void error(SAXParseException exception) throws SAXException {

    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {

    }
}
