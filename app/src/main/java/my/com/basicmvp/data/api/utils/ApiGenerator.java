package my.com.basicmvp.data.api.utils;

import android.content.Context;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import my.com.basicmvp.data.api.utils.interceptor.AuthorizeInterceptor;
import my.com.basicmvp.data.api.utils.interceptor.HeaderInterceptor;
import my.com.basicmvp.di.PerApplication;
import oauth.signpost.AbstractOAuthConsumer;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Andreas on 6/4/2017.
 */

@PerApplication
public class ApiGenerator {

    private static final int CONNECTION_TIMEOUT = 60;

    private Retrofit mRetrofit;

    public ApiGenerator(Context context, AbstractOAuthConsumer oAuthConsumer, Gson gson, String apiRootUrl) {
        mRetrofit = initRetrofit(context, oAuthConsumer, gson, apiRootUrl);
    }

    public ApiGenerator(Context context, Gson gson, String apiRootUrl) {
        mRetrofit = initRetrofit(context, null, gson, apiRootUrl);
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public <A> A createApi(Class<A> apiClass) {
        return mRetrofit.create(apiClass);
    }

    private Retrofit initRetrofit(Context context, AbstractOAuthConsumer oAuthConsumer, Gson gson,
                                  String apiRootUrl) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder()
                .addInterceptor(new HeaderInterceptor())
                .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true);

        if (oAuthConsumer != null) {
            okHttpClientBuilder.addInterceptor(new AuthorizeInterceptor(oAuthConsumer));
        }

        return new Retrofit.Builder().baseUrl(apiRootUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClientBuilder.build())
                .build();
    }
}
