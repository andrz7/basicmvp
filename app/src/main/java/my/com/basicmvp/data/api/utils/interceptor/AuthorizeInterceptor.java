package my.com.basicmvp.data.api.utils.interceptor;

import java.io.IOException;

import oauth.signpost.AbstractOAuthConsumer;
import oauth.signpost.exception.OAuthException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by Andreas on 6/4/2017.
 */

public class AuthorizeInterceptor implements Interceptor {

    private AbstractOAuthConsumer mConsumer;

    public AuthorizeInterceptor(AbstractOAuthConsumer mConsumer) {
        this.mConsumer = mConsumer;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Timber.i("Retrofit %s", request.url().toString());
        try {
            return chain.proceed((Request) mConsumer.sign(request).unwrap());
        } catch (OAuthException e) {
            throw new IOException(e);
        }
    }
}
