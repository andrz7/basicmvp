package my.com.basicmvp.data.model;

/**
 * Created by kk-mb-007 on 6/14/17.
 */

public class OAuthKey {
    private String mConsumerKey;
    private String mConsumerSecret;
    private String mApiRootUrl;

    public OAuthKey(String consumerKey, String consumerSecret, String apiRootUrl) {
        mConsumerKey = consumerKey;
        mConsumerSecret = consumerSecret;
        mApiRootUrl = apiRootUrl;
    }

    public String getConsumerKey() {
        return mConsumerKey;
    }

    public String getConsumerSecret() {
        return mConsumerSecret;
    }

    public String getApiRootUrl() {
        return mApiRootUrl;
    }
}
