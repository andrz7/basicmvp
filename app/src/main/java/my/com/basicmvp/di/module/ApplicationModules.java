package my.com.basicmvp.di.module;

import android.content.Context;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import my.com.basicmvp.Application;
import my.com.basicmvp.data.api.utils.ApiGenerator;
import my.com.basicmvp.di.PerApplication;
import oauth.signpost.AbstractOAuthConsumer;

/**
 * Created by kk-mb-007 on 6/14/17.
 */

@Module
public class ApplicationModules {
    private final String mApiRootUrl;

    public ApplicationModules(Application application) {
        this.mApiRootUrl = "";
    }

    @Provides
    @PerApplication
    ApiGenerator provideApiGenerator(Context context, AbstractOAuthConsumer oAuthConsumer, Gson gson) {
        return oAuthConsumer != null ? new ApiGenerator(context, gson, this.mApiRootUrl) :
                new ApiGenerator(context, oAuthConsumer, gson, this.mApiRootUrl);
    }
}
