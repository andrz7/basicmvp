package my.com.basicmvp.di.component;

import dagger.Component;
import my.com.basicmvp.di.PerLibrary;

/**
 * Created by kk-mb-007 on 6/14/17.
 */

@PerLibrary
@Component()
public interface LibraryComponent {}
