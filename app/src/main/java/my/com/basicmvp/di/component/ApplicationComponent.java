package my.com.basicmvp.di.component;

import dagger.Component;
import my.com.basicmvp.di.PerApplication;
import my.com.basicmvp.di.module.ApplicationModules;

/**
 * Created by kk-mb-007 on 6/14/17.
 */
@PerApplication
@Component(
        dependencies = {CommonComponent.class},
        modules = {ApplicationModules.class}
)
public interface ApplicationComponent {}
