package my.com.basicmvp.di;

/**
 * Created by Andreas on 6/4/2017.
 */

public interface HasComponent<C> {
    C getComponent();
}
