package my.com.basicmvp.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import my.com.basicmvp.di.module.CommonModule;
import my.com.basicmvp.domain.executor.PostExecutionThread;
import my.com.basicmvp.domain.executor.ThreadExecutor;
import my.com.basicmvp.rx.SchedulerComposer;

/**
 * Created by Andreas on 6/3/2017.
 */

@Singleton
@Component(modules = {
        CommonModule.class
})
public interface CommonComponent {
    Context applicationContext();
    SchedulerComposer schedulerComposer();
    ThreadExecutor threadExecutor();
    PostExecutionThread postExecutionThread();
}
