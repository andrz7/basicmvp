package my.com.basicmvp.di.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import my.com.basicmvp.domain.executor.JobExecutor;
import my.com.basicmvp.domain.executor.PostExecutionThread;
import my.com.basicmvp.domain.executor.ThreadExecutor;
import my.com.basicmvp.domain.executor.UIThread;
import my.com.basicmvp.rx.SchedulerComposer;

/**
 * Created by Andreas on 6/4/2017.
 */

@Module
public class CommonModule {

    private final Application mApplication;

    public CommonModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    SchedulerComposer provideSchedulerComposer(ThreadExecutor threadExecutor,
                                               PostExecutionThread postExecutionThread) {
        return new SchedulerComposer(threadExecutor, postExecutionThread);
    }

    @Provides
    @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides
    @Singleton
    PostExecutionThread providePostExecutionThread(UIThread uiThread) {
        return uiThread;
    }
}
